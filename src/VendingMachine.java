//Ahmed Atia
import static java.lang.Double.parseDouble;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ahmed Atia
 */
public class VendingMachine extends javax.swing.JFrame {

    /**
     * Creates new form VendingMachine
     */
    public VendingMachine() {
        initComponents();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        candyRB = new javax.swing.JRadioButton();
        cokeRB = new javax.swing.JRadioButton();
        snackRB = new javax.swing.JRadioButton();
        pepsiRB = new javax.swing.JRadioButton();
        nutsRB = new javax.swing.JRadioButton();
        sodaRB = new javax.swing.JRadioButton();
        snackQ = new javax.swing.JTextField();
        nutsQ = new javax.swing.JTextField();
        pepsiQ = new javax.swing.JTextField();
        sodaQ = new javax.swing.JTextField();
        cokeQ = new javax.swing.JTextField();
        candyQ1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        moneyTF = new javax.swing.JTextField();
        changeTF = new javax.swing.JTextField();
        purchaseBtn = new javax.swing.JButton();
        clerBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        qdisplayBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Viner Hand ITC", 1, 48)); // NOI18N
        jLabel1.setText("V3nd!n9  M@ch!ne");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Choose your Item", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18))); // NOI18N

        candyRB.setText("CANDY $10");
        candyRB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                candyRBActionPerformed(evt);
            }
        });

        cokeRB.setText("COKE $25");

        snackRB.setText("SNACK $50");

        pepsiRB.setText("PEPSI $35");

        nutsRB.setText("NUTS $90");

        sodaRB.setText("SODA $45");

        snackQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                snackQActionPerformed(evt);
            }
        });

        nutsQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nutsQActionPerformed(evt);
            }
        });

        pepsiQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pepsiQActionPerformed(evt);
            }
        });

        sodaQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sodaQActionPerformed(evt);
            }
        });

        cokeQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cokeQActionPerformed(evt);
            }
        });

        candyQ1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                candyQ1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(candyRB)
                                .addGap(90, 90, 90))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(candyQ1, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cokeRB))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pepsiRB)
                            .addComponent(snackRB)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addComponent(snackQ, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(cokeQ, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(141, 141, 141)
                        .addComponent(pepsiQ, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 75, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nutsRB)
                    .addComponent(sodaRB)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(sodaQ, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nutsQ, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(86, 86, 86))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(candyRB)
                    .addComponent(snackRB)
                    .addComponent(nutsRB))
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(snackQ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nutsQ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(candyQ1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pepsiRB)
                    .addComponent(cokeRB)
                    .addComponent(sodaRB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pepsiQ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sodaQ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cokeQ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jLabel2.setText("Money");

        jLabel3.setText("Change");

        changeTF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeTFActionPerformed(evt);
            }
        });

        purchaseBtn.setText("Purchase");
        purchaseBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purchaseBtnActionPerformed(evt);
            }
        });

        clerBtn.setText("Clear");
        clerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clerBtnActionPerformed(evt);
            }
        });

        cancelBtn.setText("Cancel");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        jLabel4.setText("Quantity");

        jLabel5.setText("Quantity");

        qdisplayBtn.setText("Display Quantities");
        qdisplayBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                qdisplayBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(243, 243, 243)
                        .addComponent(purchaseBtn)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(28, 28, 28)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(qdisplayBtn)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cancelBtn)
                                    .addComponent(clerBtn))))
                        .addGap(38, 38, 38)))
                .addGap(26, 26, 26))
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(moneyTF, javax.swing.GroupLayout.DEFAULT_SIZE, 139, Short.MAX_VALUE)
                    .addComponent(changeTF))
                .addGap(45, 45, 45)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(moneyTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(changeTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(131, 131, 131)
                        .addComponent(jLabel4)
                        .addGap(39, 39, 39)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addComponent(purchaseBtn)
                        .addGap(21, 21, 21))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(qdisplayBtn)
                                .addGap(35, 35, 35)
                                .addComponent(clerBtn))
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addComponent(cancelBtn)
                        .addGap(30, 30, 30))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void candyRBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_candyRBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_candyRBActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void clerBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clerBtnActionPerformed
        // TODO add your handling code here:
        candyRB.setSelected(false);
        snackRB.setSelected(false);
        nutsRB.setSelected(false);
        cokeRB.setSelected(false);
        pepsiRB.setSelected(false);
        sodaRB.setSelected(false);
        
        candyQ1.setText(null);
        snackQ.setText(null);
        nutsQ.setText(null);
        cokeQ.setText(null);
        pepsiQ.setText(null);
        sodaQ.setText(null);
        
        moneyTF.setText(null);
        changeTF.setText(null);
        
        
    }//GEN-LAST:event_clerBtnActionPerformed

    private void purchaseBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purchaseBtnActionPerformed
        // TODO add your handling code here:
        double money = 0;
        money = Double.parseDouble(moneyTF.getText());
        
        if (candyRB.isSelected())
        {
            money = money - (10 * Integer.parseInt(candyQ1.getText()));
        }
        
        if (snackRB.isSelected())
        {
            money = money - (50 * Integer.parseInt(snackQ.getText()));
        }
        
        if (nutsRB.isSelected())
        {
            money = money - (90 * Integer.parseInt(nutsQ.getText()));
        }
        
        if (cokeRB.isSelected())
        {
            money = money - (25 * Integer.parseInt(cokeQ.getText()));
        }
        
        if (pepsiRB.isSelected())
        {
            money = money - (35 * Integer.parseInt(pepsiQ.getText()));
        }
        
        if (sodaRB.isSelected())
        {
            money = money - (45 * Integer.parseInt(sodaQ.getText()));
        }
        
        if (money <0){
            JOptionPane.showMessageDialog(rootPane, "Sorry, You Haven't Enough Money");
            changeTF.setText(null);
        }
        else {
            changeTF.setText(Double.toString(money));
            JOptionPane.showMessageDialog(rootPane, "You paid : " + moneyTF.getText());
            JOptionPane.showMessageDialog(rootPane, "You will get : " + changeTF.getText());
            double temp = Double.parseDouble(changeTF.getText()) % Integer.parseInt(changeTF.getText());
            JOptionPane.showMessageDialog(rootPane, "You will get : " + changeTF.getText() + "\n: Get: $" + Integer.parseInt(changeTF.getText()) + "\n: and : " + temp + "coins");

            if (temp > 0.0){
//            JOptionPane.showMessageDialog(rootPane, "You will get : " + changeTF.getText() + "\n: Get: $" + Integer.parseInt(changeTF.getText()) + "\n: and : " + temp + "coins");
            JOptionPane.showMessageDialog(rootPane, "\n: Get: $" + Integer.parseInt(changeTF.getText()) + "\n: and : " + temp + "coins");
            }
            else if (temp ==0.0 ){
                JOptionPane.showMessageDialog(rootPane, "No change");
            }
            else {
                JOptionPane.showMessageDialog(rootPane, "\n: Get: $" + temp + "coins"); 
            }
            
        }
        
    }//GEN-LAST:event_purchaseBtnActionPerformed

    private void snackQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_snackQActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_snackQActionPerformed

    private void nutsQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nutsQActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nutsQActionPerformed

    private void pepsiQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pepsiQActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pepsiQActionPerformed

    private void sodaQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sodaQActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sodaQActionPerformed

    private void cokeQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cokeQActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cokeQActionPerformed

    private void qdisplayBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_qdisplayBtnActionPerformed
        // TODO add your handling code here:
        int Qcandy = 50;
        int Qsnack = 20;
        int Qnuts = 30;
        int Qcoke = 50;
        int Qpepsi = 50;
        int Qsoda = 40;
        JOptionPane.showMessageDialog(rootPane, "Quantities are :  " + "\n1: Candy :" + Qcandy + "\n2: Snack :"  + Qsnack + "\n3: Nuts : " + Qnuts + "\n4 Coke : " + Qcoke + "\n5: Pepsi : " + Qpepsi + "\n6: Soda : " + Qsoda  );
        
        int newQcandy = Qcandy - Integer.parseInt(candyQ1.getText());
        int newQsnack = Qsnack - Integer.parseInt(snackQ.getText());
        int newQnuts = Qnuts - Integer.parseInt(nutsQ.getText());
        int newQcoke = Qcoke - Integer.parseInt(cokeQ.getText());
        int newQpepsi = Qpepsi - Integer.parseInt(pepsiQ.getText());
        int newQsoda = Qsoda - Integer.parseInt(sodaQ.getText());
        JOptionPane.showMessageDialog(rootPane, "Quantities are :  " + "\n: Candy :" + newQcandy + "\n1: Snack :"  + Qsnack + "\n2: Nuts : " + Qnuts + "\n3 Coke : " + Qcoke + "\n4: Pepsi : " + Qpepsi + "\n5: Soda : " + Qsoda  );
        
        
        
    }//GEN-LAST:event_qdisplayBtnActionPerformed

    private void changeTFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeTFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_changeTFActionPerformed

    private void candyQ1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_candyQ1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_candyQ1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VendingMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VendingMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VendingMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VendingMachine.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VendingMachine().setVisible(true);
            }
        });
    }
    public class NoChangeAvailableException extends Exception {
        double internalChange1 = 500.525;
        double needed = Double.parseDouble(changeTF.getText());
        
    }
            
    public class NotSufficientPaidException extends Exception {
        double internalChange1 = 500.525;
        double needed = Double.parseDouble(changeTF.getText());
        
    }  
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelBtn;
    private javax.swing.JTextField candyQ1;
    private javax.swing.JRadioButton candyRB;
    private javax.swing.JTextField changeTF;
    private javax.swing.JButton clerBtn;
    private javax.swing.JTextField cokeQ;
    private javax.swing.JRadioButton cokeRB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField moneyTF;
    private javax.swing.JTextField nutsQ;
    private javax.swing.JRadioButton nutsRB;
    private javax.swing.JTextField pepsiQ;
    private javax.swing.JRadioButton pepsiRB;
    private javax.swing.JButton purchaseBtn;
    private javax.swing.JButton qdisplayBtn;
    private javax.swing.JTextField snackQ;
    private javax.swing.JRadioButton snackRB;
    private javax.swing.JTextField sodaQ;
    private javax.swing.JRadioButton sodaRB;
    // End of variables declaration//GEN-END:variables
}
